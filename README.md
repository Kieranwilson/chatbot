#Slack Chatbot
This is a slack chabot built off the chatbot package. It uses docker to run, has Xdebug enabled for debugging purposes
and has a watcher setup to restart the artisan command once changes are detected.

## Getting setup
Clone the repository using
```git
git clone https://bitbucket.org/Kieranwilson/chatbot.git
```
After this create the .env in both the repository root and the src folder. Now run 
```bash
composer install
```
<!-- If I end up adding a database then put it here -->
Now that we have the vender populated the chatbot is ready to go, to start the docker vms, go to the repository
root and run
```bash
docker-compose up -d
```
## Getting the database
Database dump is located in `src/database/backup.sql`
```bash
docker exec -it chatbot_php_1 composer run-script database-import
```
## Exporting the database for commit
```bash
docker exec -it chatbot_php_1 composer run-script database-export
```
## Linting
Linting has been caked into composer. To check your code run:
```bash
composer run-script php-code-sniff
```
To automatically fix most issues you can run
```bash
composer run-script php-code-fix
```
If you are having issues there is a very verbose output command that can be run with.  
<b>There will be alot of output</b>
```bash
composer run-script php-code-sniff-verbose
```

## Chatbot Command
The chatbot will run automatically after the docker container starting, using the command
```bash
/usr/local/bin/php artisan botman:listen
```

## Accessing the php machine
If you change the folder from chatbot then the name will have to be updated
```bash
docker exec -it chatbot_php_1 bash
```
to see a list of docker machines use
```bash
docker ps
```
to see the log history for the machine use
```bash
docker logs chatbot_php_1
```
Laravel logs will get written as normal

### BotMan Laravel Starter

For more information on the core botman package visit [BotMan](https://github.com/mpociot/botman).

####To edit the bots logic
Modify your bot logic in `src/routes/botman.php`.

## Extras

### Xdebug Configuration
xDebug is using remote_host configuration to talk to the host machine, this means that the remote_host must be
set correctly for your IDE to receive the connection. To do this edit the .env in the project root, changing the
`XDEBUG_HOST_IP` to your local IP address. After this it requires your IDE to be configured.

#### PHPSTORM Xdebug
Go to File->Settings and then go to `Languages & Frameworks -> PHP -> Debug` and ensure the Xdebug port is set to 9000
(which is the default port), that it can accept extenal connections, also make sure that the zend debug port is not set 
to 9000 (9001 for example).

Now go into `Languages & Frameworks -> PHP -> Debug->DBGp Proxy` and ensure that the port is
also 9000 and the host is equal to your local ip address. Save all changes.

Upon forming an Xdebug connection if you go to the debug tab you should get a message saying  
```
Can't find a sourceposition. Server with name ''aravel' doesn't exist"
Configure Servers
```
Click configure servers, create a new one named laravel, set the host to localhost, port to 9000, debugger to Xdebug
and check use path mappings. Now map the src folder to `/var/www/html`

### File Watching
I originally created a shell script which is still commited `src/autorestart.sh` this shell script makes use of
inotifywait to watch the files then kill the previous php and then start the artisan once more. This however does not
on windows file systems, to get around this I had to find another way to watch the filesystem and run a command when
changes were found.

#### GO
This led me to a watcher based on a language called go, which watches the files and is supposed to be platform
independant, it had the same issue but I edited the samefile.go file to replace it with the logic it uses on windows
filesystems which compares the file size as well. With this solution it was successful

If you need to change this configuration then you have to edit the file at `src/watch/watch.go` you have to then build
the go file by `cd /var/www/html/watch` then `go build`