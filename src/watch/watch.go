package main

import (
	"fmt"
	"log"
	"time"
	"os/exec"

	"./watcher-master"
)

func main() {
	w := watcher.New()

	go func() {
		for {
			select {
				case event := <-w.Event:
					if !event.IsDir() {
						fmt.Println("Restarting application")
						cmd := exec.Command("/var/www/html/restart.sh")
						err := cmd.Start()
		                if err != nil {
		                    log.Fatal(err)
		                }
		            }
				case err := <-w.Error:
					log.Fatalln(err)
				case <-w.Closed:
					return
			}
		}
	}()

	// Watch this folder for changes.
	if err := w.Add("/var/www/html/"); err != nil {
		log.Fatalln(err)
	}

	// Watch folders recursively for changes.
	if err := w.AddRecursive("/var/www/html/app"); err != nil {
		log.Fatalln(err)
	}
	if err := w.AddRecursive("/var/www/html/config"); err != nil {
        log.Fatalln(err)
    }
    if err := w.AddRecursive("/var/www/html/database"); err != nil {
        log.Fatalln(err)
    }
    if err := w.AddRecursive("/var/www/html/public"); err != nil {
        log.Fatalln(err)
    }
    if err := w.AddRecursive("/var/www/html/resources"); err != nil {
        log.Fatalln(err)
    }
    if err := w.AddRecursive("/var/www/html/routes"); err != nil {
        log.Fatalln(err)
    }
    if err := w.AddRecursive("/var/www/html/vendor"); err != nil {
        log.Fatalln(err)
    }

    go func() {
    		w.Wait()
    		w.TriggerEvent(watcher.Create, nil)
    	}()

	// Start the watching process - it'll check for changes every 100ms.
	if err := w.Start(time.Millisecond * 100); err != nil {
		log.Fatalln(err)
	}
}
