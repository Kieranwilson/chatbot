#!/usr/bin/env bash
service apache2 start
ngrok authtoken ${NGR_TOKEN}
nohup ngrok http -region=au -subdomain=brobot.au.ngrok.io 80 > /dev/stdout 2> /dev/stderr < /dev/null &
/var/www/html/watch/watch
