#!/bin/bash
/usr/local/bin/php /var/www/html/artisan botman:listen &
while inotifywait -r -e create -e modify -e moved_to -e moved_from -e delete /var/www/html
do
	echo 'Killing PHP'
    pkill php
    /usr/local/bin/php /var/www/html/artisan botman:listen &
done        