<?php
use App\Http\Controllers\BotManController;
use Google\Cloud\Language\LanguageClient;
use App\Conversations\FileUpload;

// Don't use the Facade in here to support the RTM API too :)
$botman = resolve('botman');

$botman->hears(
    'fetch users',
    function ($bot) {
        $bot->fetchUsers()->then(
            function ($users) use (&$bot) {
                $content = 'Current Users are: ';
                foreach ($users as $key => $user) {
                    $content
                        .= PHP_EOL . $user->data[
                        'id'] . ': ' . $user->data['name'];
                }
                $bot->reply($content);
            }
        );
    }
);

$botman->hears(
    'test',
    function ($bot) {
        $bot->reply('Hello');
    }
);

$botman->hears('Start conversation', BotManController::class . '@startConversation');

$botman->hears(
    'Tell ([a-zA-z0-9]*) (.*)',
    function ($bot, $name, $message) {
        $bot->fetchUsers()->then(
            function ($users) use (&$bot, &$name, &$message) {
                $userID = false;
                foreach ($users as $key => $user) {
                    if ($user->data['name'] == $name) {
                        $userID = $user->data['id'];
                    }
                }
                if ($userID !== false) {
                    $bot->say($message, $userID);
                } else {
                    $bot->reply('Could not locate that user');
                }
            }
        );
    }
);
//xoxb-225084322485-de1gd8jo83LDInwb9RbHeG5j

$botman->hears('%%%([^%]*)%%%', function ($bot, $fileType) {

	$file = $bot->getMessage()->getPayload()->get('file');

	$bot->startConversation(new FileUpload($file));

    //$bot->reply("Do you want to upload ${file['name']} to google drive automatically?");

	// Inside your conversationa
	/*$question = \Mpociot\BotMan\Question::create('Do you want to upload ${file[\'name\']} to google drive automatically?')
		->callbackId('file_upload')
		->addButtons([
			\Mpociot\BotMan\Button::create('Yes'),
			\Mpociot\BotMan\Button::create('No')
		]);

	$bot->ask($question, function (\Mpociot\BotMan\Answer $answer) use (&$bot) {
		$selectedOptions = $answer->getValue();
		$bot->reply($answer->getValue());
	});*/

});

$botman->hears(
    'Analyze: (.*)',
    function ($bot, $message) {
        $projectId = 'custom-point-177305';
        # Instantiates a client
        $language = new LanguageClient([
            'projectId' => $projectId
        ]);
        $annotation = $language->annotateText($message, [
            'features' => ['entities', 'syntax', 'sentiment']
        ]);

        $info = $annotation->info();

        unset($info['tokens']);


        $bot->reply(print_r($info, true));
    }
);

$botman->fallback(
	function($bot) {
		$bot->reply('fallback');
	}
);