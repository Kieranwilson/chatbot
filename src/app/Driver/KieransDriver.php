<?php
namespace App\Driver;

use Slack\File;
use Mpociot\BotMan\User;
use Slack\RealTimeClient;
use Mpociot\BotMan\Answer;
use Mpociot\BotMan\BotMan;
use Mpociot\BotMan\Message;
use Mpociot\BotMan\Question;
use Illuminate\Support\Collection;
use Mpociot\BotMan\Drivers\SlackRTMDriver;
use Mpociot\BotMan\Interfaces\DriverInterface;
use Mpociot\BotMan\Messages\Message as IncomingMessage;

class KieransDriver extends SlackRTMDriver implements DriverInterface
{
    public function fetchUsers()
    {
        return $this->client->getUsers();
    }

    public function replyAt($message, $channel, $matchingMessage = [])
    {
        $parameters = [
            'channel' => $channel,
            'as_user' => true,
        ];

        $fileToUpload = null;

        if ($message instanceof IncomingMessage) {
            $parameters['text'] = $message->getMessage();
            if (! is_null($message->getImage())) {
                $parameters['attachments'] = json_encode(
                    [['title' => $message->getMessage(), 'image_url' => $message->getImage()]]
                );
            }

            if (! empty($message->getFilePath()) && file_exists($message->getFilePath())) {
                $fileToUpload = (new File())
                        ->setTitle(basename($message->getFilePath()))
                        ->setPath($message->getFilePath())
                        ->setInitialComment($message->getMessage());
            }
        } elseif ($message instanceof Question) {
            $parameters['text']        = '';
            $parameters['attachments'] = json_encode([$message->toArray()]);
        } else {
            $parameters['text'] = $message;
        }

        if (empty($fileToUpload)) {
            $this->client->apiCall('chat.postMessage', $parameters, false, false);
        } else {
            $this->client->fileUpload($fileToUpload, [$parameters['channel']]);
        }
    }

    /**
     * @return bool
     */
    public function isBot()
    {
        return $this->event->has('bot_id') && !is_null($this->event->get('bot_id')) && $this->event->get('bot_id') !== $this->bot_id;
    }

	/**
	 * Retrieve the chat message.
	 *
	 * @return array
	 */
	public function getMessages()
	{
		$messageText = $this->event->get('text');
		$user_id = $this->event->get('user');
		$channel_id = $this->event->get('channel');

		if ($this->event->get('subtype') === 'file_share') {
			$file = Collection::make($this->event->get('file'));
			$message = new Message('', '', '');

			$message = new Message('%%%_' . $file->get('pretty_type') . '_%%%', $user_id, $channel_id, $this->event);
			$message->setAttachments([$file->get('permalink')]);

			return [$message];
		}

		return [new Message($messageText, $user_id, $channel_id, $this->event)];
	}
}
