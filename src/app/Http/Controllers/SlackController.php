<?php
/**
 * Created by PhpStorm.
 * User: Kieran W
 * Date: 11/08/2017
 * Time: 9:19 AM
 */

namespace App\Http\Controllers;

use App\Conversations\ExampleConversation;
use Illuminate\Console\Command;
use Mpociot\BotMan\BotManFactory;
use App\Factory\KieransBotFactory;
use Mpociot\BotMan\DriverManager;
use Mpociot\BotMan\Cache\ArrayCache;
use React\EventLoop\Factory;
use Illuminate\Http\Request;
use Mpociot\BotMan\BotMan;

class SlackController extends Controller
{
    protected $loop;

    public function __construct()
    {

        $app        = app('app');
        $this->loop = Factory::create();
        $loop       = &$this->loop;

        $app->singleton(
            'botman',
            function ($app) use ($loop) {
                return KieransBotFactory::createForRTM(config('services.botman', []), $loop, new ArrayCache());
            }
        );
    }

    /**
     * Place your BotMan logic here.
     */
    public function handle()
    {
        require base_path('routes/botman.php');
        $this->loop->run();
    }

    /**
     * Loaded through routes/botman.php
     *
     * @param BotMan $bot
     */
    public function startConversation(BotMan $bot)
    {
        $bot->startConversation(new ExampleConversation());
    }
}
