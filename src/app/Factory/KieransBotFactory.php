<?php

namespace App\Factory;

use Mpociot\BotMan\BotMan;
use Mpociot\BotMan\BotManFactory;
use Slack\RealTimeClient;
use Mpociot\BotMan\Http\Curl;
use Illuminate\Support\Collection;
use React\EventLoop\LoopInterface;
use Mpociot\BotMan\Cache\ArrayCache;
use App\Driver\KieransDriver;
use Mpociot\BotMan\Interfaces\CacheInterface;
use Symfony\Component\HttpFoundation\Request;
use Mpociot\BotMan\Interfaces\StorageInterface;
use Mpociot\BotMan\Storages\Drivers\FileStorage;

class KieransBotFactory extends BotManFactory
{
	public static function createForRTM(
		array $config,
		LoopInterface $loop,
		CacheInterface $cache = null,
		StorageInterface $storageDriver = null
	) {

		$client = new RealTimeClient($loop);

		return self::createUsingRTM($config, $client, $cache, $storageDriver);
	}

	public static function createUsingRTM(
		array $config,
		RealTimeClient $client,
		CacheInterface $cache = null,
		StorageInterface $storageDriver = null
	) {

		if (empty($cache)) {
			$cache = new ArrayCache();
		}

		if (empty($storageDriver)) {
			$storageDriver = new FileStorage(__DIR__);
		}

		$client->setToken(Collection::make($config)->get('slack_token'));

		$driver = new KieransDriver($config, $client);
		$botman = new BotMan($cache, $driver, $config, $storageDriver);

		$client->on(
			'message',
			function () use ($botman) {
				$botman->listen();
			}
		);

		$resolve = $client->connect()->then(
			function () use ($driver) {
				$driver->connected();
			},
			function ($error) {
				throw new \Exception($error);
			}
		);

		return $botman;
	}
}
