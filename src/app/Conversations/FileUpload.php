<?php

namespace App\Conversations;

use Illuminate\Foundation\Inspiring;
use Mpociot\BotMan\Answer;
use Mpociot\BotMan\Button;
use Mpociot\BotMan\Conversation;
use Mpociot\BotMan\Question;

class FileUpload extends Conversation {
	protected $file;

	public function __construct($file)
	{
		$this->file = $file;
	}

	public function askIfUpload()
	{
		$question = Question::create("Do you want to upload {$this->file['name']} to google drive automatically?")
			->fallback('I will not upload the file')
			->callbackId('file_upload')
			->addButtons(
				[
					Button::create('Yes')->value('yes'),
					Button::create('No')->value('no'),
				]
			);

		return $this->ask(
			$question,
			function (Answer $answer) {
				if ($answer->isInteractiveMessageReply()) {
					if ($answer->getValue() === 'yes') {
						$this->say('Okay, I will upload');
					} else {
						$this->say(Inspiring::quote());
					}
				}
			}
		);
	}

	/**
	 * Start the conversation
	 */
	public function run()
	{
		$this->askIfUpload();
	}
}